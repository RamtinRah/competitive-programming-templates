


///The famous or infamous 2-SAT problem !  the input is in format of something like : ! 1 ! 2   or ^ 1 ! 2 where ! x means not x and ^ x means x itself.




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e5 + 4;
const ll inf = 1e18;
const int mod = 1e9+7;

int n , m;
vector<int> edge[maxn] , redge[maxn];
bool mark[maxn];
vector<int> topol;
int box[maxn];
void add_edge(int u , int v){
    if(u < 0)
        u = - u + n;
    if(v < 0)
        v = - v + n;
    edge[u].pb(v);
    redge[v].pb(u);
    //cout << u << " " << v << endl;
}

void dfs(int v){
    mark[v] = true;
    for(int i=0;i<edge[v].size();i++){
        int u = edge[v][i];
        if(!mark[u])
            dfs(u);
    }
    topol.pb(v);
}
int c = 0;
void dfs1(int v){
    mark[v] = true;
    box[v] = c;
    //cout << v << " ";
    for(int i=0;i<redge[v].size();i++){
        int u = redge[v][i];
        if(!mark[u])
            dfs1(u);
    }
}
void find_scc(){
    reverse(topol.begin() , topol.end());
    for(int i=0;i<topol.size();i++){
        int v = topol[i];
        if(!mark[v]){
            c ++ ;
            dfs1(v);
            //cout << endl;
        }
    }
}

int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    cin >> n >> m;
    for(int i=0;i<m;i++)
    {
        char c1 , c2;
        int a , b;
        cin >> c1 >> a >> c2 >> b;
        if(c1 == '!')
            a *= -1;
        if(c2 == '!')
            b *= -1;
        add_edge(-a , b);
        add_edge(-b , a);
    }
    for(int i=1;i<=2*n;i++){
        if(!mark[i])
            dfs(i);
    }
    memset(mark,0,sizeof(mark));
    find_scc();
    for(int i=1;i<=n;i++){
        if(box[i] == box[n + i]){
            //cout << i << " " << box[i] << " " << box[n + i] << endl;
            cout << "NO";
            return 0;
        }
    }
    cout << "YES";
    return 0;
}
