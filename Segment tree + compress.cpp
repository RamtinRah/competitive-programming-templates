
///The famous sum segment tree problem but the length of the array is 10^9 max *at first all numbers in the array are 0*





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 4e5 + 6;
const int maxlog = 23;
const ll inf = 1e18;
const int mod = 1e9 + 7;
const ll base = 307;

struct node{
    int child1 , child2;
    ll val;
} ;
vector<node> seg;
void update(int ind , int l , int r , int x , int val){
    if(ind == (int)seg.size()){
        node x;
        x.child1 = -1 , x.child2 = -1;
        x.val = 0;
        seg.pb(x);
    }
    seg[ind].val += (ll)val;
    if(r - l < 2)
        return ;
    int mid = (l + r) / 2;
    if(x < mid){
        if(seg[ind].child1 == -1)
            seg[ind].child1 = (int)seg.size();
        update(seg[ind].child1 , l , mid , x , val);
    }
    else{
        if(seg[ind].child2 == -1)
            seg[ind].child2 = (int)seg.size();
        update(seg[ind].child2 , mid , r , x , val);
    }
}
ll get(int ind , int l , int r , int x , int y)
{
    if(y <= l || r <= x || ind == -1)
        return 0;
    if(x <= l && r <= y)
        return seg[ind].val;
    int mid = (l + r) / 2;
    return get(seg[ind].child1 , l , mid , x , y) + get(seg[ind].child2 , mid , r , x , y);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , m;
    cin >> n >> m;
    node x;
    x.child1 = -1 , x.child2 = -1 , x.val = 0;
    seg.pb(x);
    for(int i=0;i<m;i++){
        int type;
        cin >> type;
        if(type == 1){
            int v , val;
            cin >> v >> val;
            v -- ;
            update(0 , 0 , mod , v , val);
        }
        else{
            int l , r;
            cin >> l >> r;
            l -- ;
            cout << get(0 , 0 , mod , l , r) << endl;
        }
    }
    return 0;
}
