
/// This program will calculate distance between nodes 1 and n in undirected weighted graph


#include <bits/stdc++.h>
using namespace std;
#define mp make_pair
#define pb push_back
typedef long long ll;
typedef pair<int,ll> pil;
const int maxn=1e5+6;
const ll inf=1e18+3;
vector<pil> edge[maxn];
ll dis[maxn];
set<pair<ll,int> >st;
vector<int> ans;
void pre()
{
    for(int i=2;i<maxn;i++)
        dis[i]=inf;
    st.insert(mp(0,1));
}
void dij()
{
    while(st.size())
    {
        int u= (*st.begin()).second;
        st.erase(st.begin());
        for(int i=0;i<edge[u].size();i++)
        {
            int v=edge[u][i].first;
            ll w=edge[u][i].second;
            if(dis[u] + w < dis[v])
            {
                st.erase(mp(dis[v],v));
                dis[v] = dis[u] + w;
                st.insert(mp(dis[v],v));
            }
        }
    }
}
int main()
{
    int n,m; cin>>n>>m;
    for(int i=0;i<m;i++)
    {
        int u,v,w; cin>>u>>v>>w;
        edge[u].pb(mp(v,w));
        edge[v].pb(mp(u,w));
    }
    pre();
    dij();
    cout << dis[n];
    return 0;
}
