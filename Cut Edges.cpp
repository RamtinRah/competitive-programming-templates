

/// This program will output the IDs of cut edges in an undirected graph


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 5e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9+9;
struct edge{
    int u , v ;
    bool bridge;
} ed[maxn];
int n , m , co;
vector<int> adj[maxn];
int st[maxn] , h[maxn];
bool mark[maxn];
int other(int id , int v)
{
    if(v == ed[id].v)
        return ed[id].u ;
    else
        return ed[id].v ;
}
void dfs(int v , int parid)
{
    mark[v] = true;
    st[v] = co ++ ;
    h[v] = st[v];
    for(int i=0;i<adj[v].size();i++)
    {
        int id = adj[v][i];
        if(id == parid)
            continue;
        int u = other(id , v);
        //cout << u << " " << v << endl;
        if(!mark[u])
            dfs(u , id);
        h[v] = min(h[v] , st[u]);
        if(st[u] > st[v])
        {
            h[v] = min(h[v] , h[u]);
            if(h[u] == st[u])
                ed[id].bridge = true;
        }
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    cin >> n >> m;
    for(int i=0;i<m;i++)
    {
        int u , v ;
        cin >> u >> v;
        ed[i].u = u , ed[i].v = v , ed[i].bridge = false;
        adj[u].pb(i) , adj[v].pb(i);
    }
    for(int i=1;i<=n;i++)
        if(!mark[i])
            dfs(i , -1);
    for(int i=0;i<m;i++)
        if(ed[i].bridge == true)
            cout << i + 1 << " ";
    return 0;
}
