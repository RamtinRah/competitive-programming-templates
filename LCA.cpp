
///Implementation of last common ancestor of two nodes in a tree



#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e5 + 6;
const int maxlog = 20;
const ll inf = 1e18;
const int mod = 1e9 + 7;
const ll base = 307;
vector<int> edge[maxn];
int par[maxn][maxlog + 1]  , depth[maxn];
void dfs(int v , int p)
{
    par[v][0] = p;
    depth[v] = depth[p] + 1;
    for(int i=1;i<maxlog;i++)
    {
        int u = par[v][i-1];
        par[v][i] = par[u][i-1];
    }
    for(int i=0;i<edge[v].size();i++)
    {
        int u = edge[v][i];
        if(u != p)
            dfs(u,v);
    }
}
int lca(int u,int v)
{
    if(depth[u] < depth[v])
        swap(u,v);
    for(int i=maxlog;i>=0;i--)
    {
        if(depth[par[u][i]] >= depth[v])
            u = par[u][i];
    }
    if(u == v)
        return u;
    for(int i=maxlog;i>=0;i--)
    {
        if(par[u][i] != par[v][i])
        {
            u = par[u][i];
            v = par[v][i];
        }
    }
    return par[u][0];
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , q;
    cin >> n >> q;
    for(int i=2;i<=n;i++){
        int p ;
        cin >> p;
        edge[p].pb(i) , edge[i].pb(p);
    }
    dfs(1 , 0);
    while(q -- ){
        int u , v;
        cin >> u >> v;
        cout << lca(u , v) << endl;
    }
    return 0;
}
