

///This program will calculate the maximum matching in a bipartie graph with n nodes on one side and m nodes on the other side.
/// The input format of edges is : u v where u is a node from one side and v is node from other side   1 <= u <= n   and  1 <= v <= m








#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 3e2 + 4;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9+9;
int match[maxn];
vector<int> edge[maxn];
bool mark[maxn];
int n , m , q;
bool dfs(int v)
{
    if(v == -1)
        return true;
    if(mark[v])
        return false;
    mark[v] = true;
    for(int i=0;i<edge[v].size();i++)
    {
        int u = edge[v][i];
        if(dfs(match[u]))
        {
            match[u] = v;
            return true;
        }
    }
    return false;
}
void find_match()
{
    for(int i=1;i<=m;i++)
        match[i] = -1;
    int ans = 0;
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=n;j++)
            mark[j] = false;
        ans += dfs(i);
    }
    cout << ans ;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    cin >> n >> m >> q;
    for(int i=0;i<q;i++)
    {
        int u , v;
        cin >> u >> v;
        edge[u].pb(v);
    }
    find_match();
    return 0;
}
